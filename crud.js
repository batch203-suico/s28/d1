// CRUD Operations 
/*
	C - Create (Insert document)
	R - Read/Retrieve (View specific document/s)
	U - Update (Edit specific document/s)
	D - Delete (Remove specific document/s)

	- CRUD Operations are the heart of any backend application.
*/

// [SECTION] Insert a document (Create)

/*
	-Syntax:
		- db.collectionName.insertOne({object});
	Comparison with javascript 
		object.object.method({object});
		arrayName.forEach(variable);
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gamil.com"
	},
	courses: ["CSS", "JavaScript", "Phython"],
	department: "none"
})

/*
	Mini Activity

            Scenario: We will create a database that will simulate a hotel database.
            1. Create a new database called "hotel".
            2. Insert a single room in the "rooms" collection with the following details:
               
                name - single
                accommodates - 2
                price - 1000
                description - A simple room with basic necessities
                rooms_available - 10
                isAvailable - false

            3. Use the "db.getCollection('users').find({})" query to check if the document is created.
           
            4. Take a screenshot of the Robo3t result and send it to the batch hangouts.
*/

// Insert a single document
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with basic necessities",
	rooms_available: 10,
	isAvailable: false
})

// Insert many
/*
	-Syntax
		- db.collectionName.insertMany([{objectA, {objectB}}]);
*/

db.users.insertMany([
	{firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "87654321",
		email: "stephenhawking@gamil.com"
	},
	courses: ["Phython", "React", "PHP"],
	department: "none"},

	{firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "87654321",
		email: "neilarmstrong@gamil.com"
	},
	courses: ["Phython", "React", "PHP"],
	department: "none"}
	]);


// [SECTION] Retrieve a document (Read)

/*
	-Syntax:
		-db.collectionName.find({});
		-db.collectionName.find({field:value});
*/

db.users.find({});
db.users.find({firstName:"Stephen"});
db.users.find({department:"None"});

// Find documents with multiple parameters.
/*
	-Syntax:
		- db.collectionName.find({fieldA:valueA}, {fieldB:valueB});
*/

db.users.find({lastName: "Armstrong"}, {age: 82});

// [SECTION] Updating documents (Update)

// Create a document to update 
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gamil.com"
	},
	course: [],
	department: "none"
})

/*
	-Syntax:
		- db.collectionName.updateOne({criteria}, {$set: 
		{field: value}});
*/

db.users.updateOne({
	firstName: "Test"},
	{
		$set:{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123145678",
				email: "bill@gmail.com"
			},
			course: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"

		}
	}
);

// Updating multiple documents
/*
	- Syntax:
	- db.collectionName.updateMany({criteria}, {*$set: {field:value}});
*/

db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	})

// Replace One 

/*
	- Can be used if replacing the whole document if necessary.
	-Syntax:
		- db.collectionName.replaceOne({criteria}, {field: value}});

*/

	db.users.replaceOne(
		{firstName: "Bill"},
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123145678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	);

	/*
    Mini Activity:

        1. Using the hotel database, update the queen room and set the available rooms to zero.

        2. Use the find query to validate if the room is successfully updated.

        3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/
	// update a document

	db.rooms.updateOne(
		{name: "queen"},
		{
			$set:{rooms_available: 0}

			}
	);

db.rooms.find({name: "queen"});

// [SECTION] Removing documents [DELETE]

// Deleting a single document 
/*
	-Syntax:
		- db.collectionName.deleteOne({criteria}); // deletes the first document found.
*/

	db.users.deleteOne({name: "Test"});

// Delete Many
	/*
		- Be careful when using "deleteMany" method. If no search criteria is provided, it will delete all documents in the collection.
		- Synatx:
			- db.collectionName.deleteMany({criteria});
			- db.collectionName.deleteMany({}); // DO NOT USE
	*/

	db.users.deleteMany({name: "Test"});


	//Deleting multiple documents
	db.rooms.deleteMany({rooms_available: 10,});

	//Check if the 0 available room is deleted.
	db.room.find();

	//[SECTION] Advanced queries
	/*

	*/

	// Query an embedded document 

	db.users.find({
		contact:{
			phone : "87654321",
        	email : "stephenhawking@gamil.com" 
		}
	});

	// Query on nested field 
	// dot notation also works accessing a nested field
	db.users.find({
		"contact.email": "stephenhawking@gamil.com"
	});

	// Querying an array with Exact Element 
	db.users.find(
		{
			courses: ["CSS", "JavaScript", "Phython"]
		}
	);

	// Querying an array disregarding the array element order.
	// $all matches documents where the field contains nested array elements.
	db.users.find(
		{
			courses: {$all: ["JavaScript", "CSS",  "Phython"]}
		}
	);

	// will retrieve two documents containing the "phython" as element of the nested array.
	db.users.find(
		{
			courses: {$all: ["Phython"]}
		}
	);

	// Querying an Embedded Array 

	db.users.insertOne({
		nameArr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}]
	});

	db.users.find({
		nameArr: {
			nameA: "Juan"
		}
	})